package com.ses.automationcase.utils;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

public class TestListeners implements ITestListener {

    @Override
    public void onTestStart(ITestResult result) {
        String message = String.format("Step %d) %s", result.getMethod().getPriority(), result.getMethod().getDescription());
        com.ses.automationcase.utils.LogGenerator.info(message);
        com.ses.automationcase.utils.ReporterManager.createNode(result.getMethod().getDescription());
    }

    @Override
    public void onTestSuccess(ITestResult result) {
        String message = "Step Result: SUCCESS";
        com.ses.automationcase.utils.LogGenerator.info(message);
        com.ses.automationcase.utils.ReporterManager.pass(message);
    }

    @Override
    public void onTestFailure(ITestResult result) {
        String message = "Step Result: FAILURE";
        String cause = result.getThrowable().getMessage();
        com.ses.automationcase.utils.LogGenerator.error(message + " - Cause: " + cause);
        Throwable exception = result.getThrowable();
        com.ses.automationcase.utils.ReporterManager.fail("Step Result: FAILURE", exception);
    }

    @Override
    public void onTestSkipped(ITestResult result) {
        String message = "Step Result: TEST SKIPPED";
        com.ses.automationcase.utils.LogGenerator.warn(message);
        com.ses.automationcase.utils.ReporterManager.skip(message);
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
        ITestListener.super.onTestFailedButWithinSuccessPercentage(result);
    }

    @Override
    public void onTestFailedWithTimeout(ITestResult result) {
        ITestListener.super.onTestFailedWithTimeout(result);
    }

    @Override
    public void onStart(ITestContext context) {
        String message = String.format("###### Starting Test %s ######", context.getName());
        com.ses.automationcase.utils.LogGenerator.debug(message);
    }

    @Override
    public void onFinish(ITestContext context) {
        String message = String.format("###### Closing Test %s ######", context.getName());
        com.ses.automationcase.utils.LogGenerator.debug(message);
    }
}
