package com.ses.automationcase.tests;

import org.testng.annotations.Test;

public class GoogleTest extends BaseTest {

    @Test(priority = 1, description = "Search selenium page")
    public void searchSeleniumPage() {
        googlePage.searchByGoogle("selenium");
    }

    @Test(priority = 2, description = "Click selenium link", dependsOnMethods = "searchSeleniumPage")
    public void clickingSeleniumPage() {
        seleniumPage = googlePage.clickSeleniumLink();
    }
}
