package com.ses.automationcase.tests;

import org.testng.annotations.Test;

public class SeleniumTest extends BaseTest {

    @Test(priority = 1, description = "Click selenium documentation", dependsOnMethods = "com.ses.automationcase.tests.GoogleTest.clickingSeleniumPage")
    public void clickingDocumentation() {
        seleniumPage.clickDocumentation();
    }
}
