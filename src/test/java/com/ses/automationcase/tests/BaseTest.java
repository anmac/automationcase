package com.ses.automationcase.tests;

import com.ses.automationcase.base.BasePage;
import com.ses.automationcase.pages.GooglePage;
import com.ses.automationcase.pages.SeleniumPage;
import com.ses.automationcase.utils.LogGenerator;
import com.ses.automationcase.utils.PropertiesManager;
import com.ses.automationcase.utils.ReporterManager;
import com.ses.automationcase.utils.ScreenManager;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.IResultMap;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.*;

public class BaseTest {

    private static String currentClassName;
    private static WebDriver driver;
    protected static BasePage basePage;
    protected static GooglePage googlePage;
    protected static SeleniumPage seleniumPage;

    @BeforeSuite
    public void beforeSuite() {
        // Create Report
        ReporterManager.createExtentReport();
        // Setting Properties
        PropertiesManager.setProperty("date", PropertiesManager.dateFormatted);
        PropertiesManager.setProperty("time", PropertiesManager.timeFormatted);
        PropertiesManager.setProperty("timestamp", PropertiesManager.timestamp);
    }

    @AfterSuite
    public void afterSuite() {
        ReporterManager.saveTest();
    }

    @BeforeTest
    @Parameters({"URL"})
    public void beforeTest(String url) {
        System.setProperty("webdriver.chrome.driver", "/usr/bin/chromedriver");
//        WebDriverManager.chromedriver().driverVersion("114.0.5735.90").setup();
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--headless");
        options.addArguments("--no-sandbox");
        options.addArguments("--disable-dev-shm-usage");
        ChromeDriverService service = new ChromeDriverService.Builder().withLogOutput(System.out).build();
        driver = new ChromeDriver(service, options);
        driver.manage().window().maximize();
        driver.get(url);
        basePage = new BasePage();
        basePage.setDriver(driver);
        googlePage = new GooglePage();
    }

    @AfterTest
    public void afterTest() {
        driver.quit();
    }

    @BeforeClass
    public void beforeClass() {
        String[] currentClassArr = this.getClass().getName().split("\\.");
        currentClassName = currentClassArr[currentClassArr.length - 1];
        // Logs
        String message = String.format("Loading CP %s ...", currentClassName);
        LogGenerator.info(message);
        // Creating&Adding test to existing report
        ReporterManager.createTest(currentClassName);
    }

    @AfterClass
    public void afterClass(ITestContext context) {
        // Logs
        IResultMap result = context.getFailedTests();
        String message = String
                .format("Closing CP %s", currentClassName);
        // Adding final result to the test
        if (result.size() == 0) {
            LogGenerator.info(message + " With Result SUCCESS");
            ReporterManager.testPass("Result: <b>SUCCESS</b>");
        } else {
            LogGenerator.error(message + " With Result FAILURE");
            ReporterManager.testFail("Result: <b>FAILURE</b>");
        }
    }

    @BeforeMethod
    public void setUp() {
    }

    @AfterMethod
    public void tearDown(ITestResult testResult) {
        ScreenManager.takeScreenshot(testResult, driver);
        ReporterManager.addScreenCaptureFrom(testResult);
    }
}
