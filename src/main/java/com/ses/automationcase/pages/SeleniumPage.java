package com.ses.automationcase.pages;

import com.ses.automationcase.base.BasePage;
import org.openqa.selenium.By;

public class SeleniumPage extends BasePage {

    public void clickDocumentation() {
        By locator = By.linkText("Documentation");
        super.click(locator);
    }
}
