package com.ses.automationcase.pages;

import com.ses.automationcase.base.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

public class GooglePage extends BasePage {

    public void searchByGoogle(String text) {
        By locator = By.name("q");
        clear(locator);
        setText(locator, text);
        actions().sendKeys(Keys.ENTER).perform();
    }

    public SeleniumPage clickSeleniumLink() {
        By locator = By.cssSelector("#search h3");
        super.click(locator);
        return new SeleniumPage();
    }
}
