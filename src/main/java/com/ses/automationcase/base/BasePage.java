package com.ses.automationcase.base;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

import java.time.Duration;

public class BasePage {

    private static WebDriver driver;

    public void setDriver(WebDriver driver) {
        BasePage.driver = driver;
    }

    protected WebElement find(By locator) {
        return wait(10000).until(ExpectedConditions.presenceOfElementLocated(locator));
    }

    protected void clear(By locator) {
        find(locator).clear();
    }

    protected void setText(By locator, String text) {
        find(locator).sendKeys(text);
    }

    protected void click(By locator) {
        find(locator).click();
    }

    protected Wait<WebDriver> wait(int timeoutInMilliseconds) {
        return new FluentWait<>(BasePage.driver)
                .withTimeout(Duration.ofMillis(timeoutInMilliseconds))
                .pollingEvery(Duration.ofMillis(200))
                .ignoring(NoSuchElementException.class);
    }

    protected Actions actions() {
        return new Actions(BasePage.driver);
    }
}
