FROM maven:3.9.3-eclipse-temurin-17-alpine

WORKDIR /root/automationCase

RUN apk update && apk add util-linux-misc neovim
RUN apk add chromium-chromedriver

COPY . .
